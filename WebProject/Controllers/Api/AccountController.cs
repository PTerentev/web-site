﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Interfaces;
using WebProject.Exceptions;
using WebProject.Services;
using WebProject.ViewModels;

namespace WebProject.Controllers.Api
{
    [Route("api")]
    [ApiController]
    public class AccountController : Controller
    {
        readonly ILogger<AccountController> _logger;
        readonly UserApiService _userService;
        public AccountController(ILogger<AccountController> logger, UserApiService userService)
        {
            _logger = logger ?? throw new ArgumentNullException();
            _userService = userService ?? throw new ArgumentNullException();

            _logger.LogDebug("NLog injected into ApiController");
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login(JwtUserModel model)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + "\n");
                return BadRequest(allErrors);
            }

            try
            {
                var result = await _userService.GetJwTokenAsync(model);
                return StatusCode(201, result);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("gettoken")]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> GetToken()
        {
            try
            {
                var result = await _userService.GetJwTokenAsync(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).First().Value);
                return StatusCode(201, result);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("register")]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if(!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + "\n");
                return BadRequest(allErrors);
            }

            try
            {
                _logger.LogInformation($"Trying to register new user {model.Username} || {model.Email}");
                var result = await _userService.RegisterUserAsync(model);
                if (!string.IsNullOrEmpty(result))
                    return StatusCode(201, result);

                return BadRequest();
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("change")]
        public async Task<ActionResult> Change(ChangeUserModel model)
        {       
            try
            {
                var username = User.Claims.Where(c => c.Type == ClaimTypes.Name).First().Value;

                var result = await _userService.ChangeUserAsync(username, model);
                if (result)
                    return Ok();
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }

            return BadRequest();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("user")]
        public async Task<IActionResult> GetUser()
        {         
            try
            {
                var model = await _userService.GetUserInfoAsync(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).First().Value);
                return Json(model);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }

        }
    }
}
