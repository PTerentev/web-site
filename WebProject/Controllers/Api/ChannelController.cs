﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.Functions;
using WebProject.Exceptions;
using WebProject.Services;
using WebProject.ViewModels;

namespace WebProject.Controllers.Api
{
    [Route("api")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ChannelController: Controller
    {
        readonly ChannelFunctions _functions;
        readonly ILogger<ChannelController> _logger;
        readonly UserApiService _userService;
        public ChannelController(ChannelFunctions functions, ILogger<ChannelController> logger, UserApiService userService)
        {
            _functions = functions ?? throw new ArgumentNullException();
            _logger = logger ?? throw new ArgumentNullException();
            _userService = userService ?? throw new ArgumentNullException();

            _logger.LogDebug("NLog injected into ChannelController");
        }

        [HttpGet("members")]
        public async Task<IActionResult> GetMembersInfo(int channelid)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var info = await _functions.GetMembersInfo<UserRole>(channelid, userid);
                return Json(info);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("subscribed")]
        public async Task<IActionResult> GetSubscribedChannels()
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var channels = await _functions.GetSubscribedChannelsInfoAsync<ChannelModel>(userid);
                return Json(channels);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("channels")]
        public async Task<IActionResult> GetChannels()
        {
            try
            {
                var channels = await _functions.GetChannelsInfoAsync<ChannelModel>();
                return Json(channels);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("channel")]
        public async Task<IActionResult> GetChannel(int id)
        {
            try
            {
                var channels = await _functions.GetChannelInfoAsync<ChannelModel>(id);
                return Json(channels);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("make")]
        public async Task<IActionResult> MakeChannel(CreateChannelModel model)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + "\n");
                return BadRequest(allErrors);
            }

            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var result = await _functions.MakeNewChannelAsync(model, userid);
                if (result)
                    return StatusCode(201);

                return BadRequest();
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("subscribe")]
        public async Task<IActionResult> Subscribe(int channelId)
        {  
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var result = await _functions.SubscribeToChannelAsync(channelId, userid);
                if (result)
                    return StatusCode(201);

                return BadRequest();
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("role")]
        public async Task<IActionResult> ChangeChannelRole(UserChangeRoleModel model)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + "\n");
                return BadRequest(allErrors);
            }

            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var result = await _functions.ChangeUserRoleAsync(model, userid);
                if (result)
                    return StatusCode(201);

                return BadRequest();
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }
    }
}
