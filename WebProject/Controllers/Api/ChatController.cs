﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Functions;
using WebProject.Exceptions;
using WebProject.Services;

namespace WebProject.Controllers.Api
{
    [Route("api")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ChatController: Controller
    {
        readonly ChatFunctions _functions;
        readonly ILogger<ChatController> _logger;
        readonly UserApiService _userService;
        public ChatController(ChatFunctions functions, ILogger<ChatController> logger, UserApiService userService)
        {
            _functions = functions ?? throw new ArgumentNullException();
            _logger = logger ?? throw new ArgumentNullException();
            _userService = userService ?? throw new ArgumentNullException();

            _logger.LogDebug("NLog injected into ChannelController");
        }

        [AllowAnonymous]
        [HttpGet("chats")]
        public async Task<IActionResult> Chats(int channelid)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var ids = await _functions.GetIdChatsInTheChannel(channelid, userid);
                return Json(ids);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("chat")]
        public async Task<IActionResult> Chat(int channelid, string name)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                await _functions.AddChatAsync(channelid, userid, name);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpDelete("chat")]
        public async Task<IActionResult> DeleteChat(int chatid)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                await _functions.RemoveChatAsync(chatid, userid);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("block")]
        public async Task<IActionResult> BlockUser(int chatid, string userid)
        {
            try
            {
                var moderid = _userService.GetUserIdByPrincipal(User);
                await _functions.BlockUserInTheChatAsync(chatid, moderid, userid);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("unblock")]
        public async Task<IActionResult> UnBlockUser(int chatid, string userid)
        {
            try
            {
                var moderid = _userService.GetUserIdByPrincipal(User);
                await _functions.UnBlockUserInTheChatAsync(chatid, moderid, userid);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }
    }
}
