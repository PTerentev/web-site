﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Functions;
using WebProject.Exceptions;
using WebProject.Services;
using WebProject.ViewModels;

namespace WebProject.Controllers.Api
{
    [Route("api")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MessageController: Controller
    {
        readonly MessageFunctions _functions;
        readonly ILogger<MessageController> _logger;
        readonly UserApiService _userService;
        public MessageController(MessageFunctions functions, ILogger<MessageController> logger, UserApiService userService)
        {
            _functions = functions ?? throw new ArgumentNullException();
            _logger = logger ?? throw new ArgumentNullException();
            _userService = userService ?? throw new ArgumentNullException();

            _logger.LogDebug("NLog injected into ChannelController");
        }

        [AllowAnonymous]
        [HttpGet("messages/{chatid}")]
        public async Task<IActionResult> AllMessages(int chatid)
        {
            try
            {
                var userId = _userService.GetUserIdByPrincipal(User);
                var messages = await _functions.GetMessagesFromTheChat<MessageModel>(userId, chatid);
                return Json(messages);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("messages/{chatid}/{lastmessageid}")]
        public async Task<IActionResult> AllMessages(int chatid, int lastmessageid)
        {
            try
            {
                var userId = _userService.GetUserIdByPrincipal(User);
                var messages = await _functions.GetMessagesFromTheChat<MessageModel>(userId, chatid, lastmessageid);
                return Json(messages);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("message")]
        public async Task<IActionResult> AddMessage(int chatid, string message)
        {
            try
            {
                var userId = _userService.GetUserIdByPrincipal(User);
                await _functions.SendMessage(userId, chatid, message);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }
    }
}
