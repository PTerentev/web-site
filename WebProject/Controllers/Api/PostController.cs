﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.Functions;
using WebProject.DataBase.Interfaces;
using WebProject.Exceptions;
using WebProject.Services;
using WebProject.ViewModels;

namespace WebProject.Controllers.Api
{
    [Route("api")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PostController: Controller
    {
        readonly PostFunctions _functions;
        readonly ILogger<PostController> _logger;
        readonly UserApiService _userService;
        public PostController(PostFunctions functions, ILogger<PostController> logger, UserApiService userService)
        {
            _functions = functions ?? throw new ArgumentNullException();
            _logger = logger ?? throw new ArgumentNullException();
            _userService = userService ?? throw new ArgumentNullException();

            _logger.LogDebug("NLog injected into PostController");
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddPost(AddPostModel model)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + "\n");
                return BadRequest(allErrors);
            }

            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                await _functions.AddPostAsync(model.ChannelId, userid, model.Text);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("likeorcancellike")]
        public async Task<IActionResult> LikeOrCancelPost(int postId, bool like)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                await _functions.LikeOrCancelLikePostAsync(postId, userid, like);
                return StatusCode(201);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("post")]
        public async Task<IActionResult> GetPost(int postid)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var post = await _functions.GetPostAsync<PostModel>(postid, userid);
                return Json(post);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("posts")]
        public async Task<IActionResult> GetPosts(int channelid)
        {
            try
            {
                var userid = _userService.GetUserIdByPrincipal(User);
                var post = await _functions.GetAllPostsFromChannelAsync<PostModel>(channelid, userid);
                return Json(post);
            }
            catch (IncorrectDataFormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500);
            }
        }
    }
}
