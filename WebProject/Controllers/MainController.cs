﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Functions;
using WebProject.Services;
using WebProject.ViewModels;

namespace WebProject.Controllers
{
    public class MainController : Controller
    {
        readonly ILogger<MainController> _logger;
        readonly SignInManager<ApplicationUser> _signInManager;
        readonly UserManager<ApplicationUser> _userManager;
        public MainController(ILogger<MainController> logger,
            UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(UserManager<ApplicationUser>));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(SignInManager<ApplicationUser>));
            _logger = logger ?? throw new ArgumentNullException();

            _logger.LogDebug("NLog injected into ApiController");
        }

        [HttpGet]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Landing");
                }
            }

            return Unauthorized();
        }

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = 12000, NoStore = false, Order = 2)]
        public IActionResult Landing()
        {
            return Json("Hi!");
        }

    }
}
