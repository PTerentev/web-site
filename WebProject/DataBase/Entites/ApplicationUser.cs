﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Entites
{
    public class ApplicationUser: IdentityUser
    {
        [ProtectedPersonalData]      
        [MaxLength(70)]
        [MinLength(3)]
        [Required]
        public override string UserName { get; set; }

        [ProtectedPersonalData]
        [DataType(DataType.EmailAddress)]
        [Required]
        public override string Email { get; set; }

        [MaxLength(70)]
        public string FirstName { get; set; }
        [MaxLength(70)]

        [ProtectedPersonalData]
        public string LastName { get; set; }
        public string AvatarImage {get;set;}
        [ProtectedPersonalData]
        public DateTime DateOfBirth { get; set; }
        public DateTime RegDate { get; set; } = DateTime.Now;

        public List<UserPostLike> WhichLiked { get; set; }
    }
}
