﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;

namespace WebProject.DataBase.Entites
{
    public class Channel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string About { get; set; }
        [Required]
        public Access Access { get; set; }
        public DateTime RegDate { get; set; } = DateTime.Now;
        [ConcurrencyCheck]
        public List<Post> Posts { get; set; } = new List<Post>();
        [ConcurrencyCheck]
        public List<Chat> Chats{ get; set; } = new List<Chat>();
    }
}
