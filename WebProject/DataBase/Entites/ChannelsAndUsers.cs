﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;

namespace WebProject.DataBase.Entites
{
    public class ChannelsAndUsers
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        [Required]
        public ApplicationUser User { get; set; }
        public int ChannelId { get; set; }
        [Required]
        public Channel Channel { get; set; }
        [Required]
        public Roles Role { get; set; }

        public override string ToString()
        {
            return $"Юзер - {UserId}, Канал - {Channel.Id}, Роль - {Role.ToString()}";
        }
    }
}
