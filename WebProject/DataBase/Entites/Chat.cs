﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Entites
{
    public class Chat
    {
        public Chat()
        {
            Messages = new List<Message>();
            BlockedUsers = new List<UserChatBlock>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int ChannelId { get; set; }
        [Required]
        public Channel Channel { get; set; }
        public int? OwnerId { get; set; }
        public ApplicationUser Owner { get; set; }

        [ConcurrencyCheck]
        public List<Message> Messages { get; set; }
        public List<UserChatBlock> BlockedUsers { get; set; }
    }
}
