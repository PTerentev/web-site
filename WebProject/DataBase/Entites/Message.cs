﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Entites
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        public string SenderId { get; set; }
        public ApplicationUser Sender { get; set; }

        public int ChatId { get; set; }
        public Chat Chat { get; set; }

        [Required]
        public string Text { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
