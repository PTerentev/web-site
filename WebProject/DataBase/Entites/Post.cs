﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Entites
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
        public int ChannelId { get; set; }
        [Required]
        public Channel Channel { get; set; }
        [Required]
        public string Text { get; set; }
        public int CountViewers { get; set; } 
        public List<UserPostLike> WhoLiked { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;

    }
}
