﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Enums
{
    public enum Access
    {
        Public = 1,
        Private = 2,
    }
}
