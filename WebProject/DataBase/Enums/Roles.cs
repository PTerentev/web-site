﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Enums
{
    public enum Roles
    {
        Subscriber = 1,
        Moderator = 2,
        Admin = 3,
        Desirous = 4
    }
}
