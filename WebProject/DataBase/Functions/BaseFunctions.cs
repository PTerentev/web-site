﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Enums;
using WebProject.Exceptions;

namespace WebProject.DataBase.Functions
{
    public abstract class BaseFunctions
    {
        protected readonly ApplicationDbContext _db;
        protected readonly UserManager<ApplicationUser> _userManager;
        protected readonly IMemoryCache _memoryCache;
        protected const int ExpirationCacheTime = 2;
        public BaseFunctions(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IMemoryCache memoryCache)
        {
            if (db == null || userManager == null || memoryCache == null)
                throw new ArgumentNullException();

            _db = db;
            _userManager = userManager;
            _memoryCache = memoryCache;
        }

        protected async Task<bool> CheckUserRightsAsync(int channelId, string userId, Roles role)
        {
            if (!await _db.Channels.AnyAsync(c => c.Id == channelId))
                throw new IncorrectDataFormatException("Incorrect channel Id!");

            var cAndu = await _db.ChannelsAndUsers.Where(c => c.UserId == userId && c.ChannelId == channelId).FirstOrDefaultAsync();
            if (cAndu == null)
                return false;

            return cAndu.Role switch
            {
                Roles.Admin => true,
                Roles.Moderator => role == Roles.Subscriber || role == Roles.Moderator,
                Roles.Subscriber => role == Roles.Subscriber,
                Roles.Desirous => role == Roles.Desirous,
                _ => throw new Exception($"Current function class does not support such role! Role - {cAndu.Role}"),
            };
        }
    }
}
