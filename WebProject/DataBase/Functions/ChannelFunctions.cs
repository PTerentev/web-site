﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Interfaces;
using WebProject.DataBase.Enums;
using WebProject.Exceptions;
using Microsoft.Extensions.Caching.Memory;

namespace WebProject.DataBase.Functions
{
    /// <summary>
    /// Операции над каналами.
    /// </summary>
    /// /// <remarks>
    /// Этот класс может добавить канал, назначить роли юзерам, осуществлять подписку на канал.
    /// </remarks>
    public class ChannelFunctions: BaseFunctions
    {
        public ChannelFunctions(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IMemoryCache memoryCache) : base(db, userManager, memoryCache)
        {

        }

        public async Task<IEnumerable<IUserRole>> GetMembersInfo<Model>(int channelId, string moderId)
            where Model: class, IUserRole, new()
        {

            if (!await CheckUserRightsAsync(channelId, moderId, Roles.Subscriber))
                throw new IncorrectDataFormatException("User does not have rights to do this!");

            return await _db.ChannelsAndUsers.
                Where(c => c.ChannelId == channelId).
                Select(c =>
                        new Model()
                        {
                            UserId = c.UserId,
                            Role = c.Role
                        }).ToListAsync();
        }

        public async  Task<List<Model>> GetChannelsInfoAsync<Model>()
            where Model : class, IChannelModel, new()
        {
            return await _db.Channels.Select(c => new Model
            {
                Id = c.Id,
                Name = c.Name,
                About = c.About,
                Access = c.Access
            }).ToListAsync();
        }

        public async Task<List<Model>> GetSubscribedChannelsInfoAsync<Model>(string userId)
            where Model : class, IChannelModel, new()
        {
            var channelsId = await _db.ChannelsAndUsers.Where(c => c.UserId == userId).Select(c => c.ChannelId).ToListAsync();

            return await _db.Channels.Where(c => channelsId.Contains(c.Id)).Select(c => new Model
            {
                Id = c.Id,
                Name = c.Name,
                About = c.About,
                Access = c.Access,
                SubscribersCount = _db.ChannelsAndUsers.Count(s => s.ChannelId == c.Id)
            }).ToListAsync();
        }

        public async Task<Model> GetChannelInfoAsync<Model>(int idChannel)
            where Model : class, IChannelModel, new()
        {
            var channel = await getChannelByChannelIDAsync(idChannel);
            if (channel == null)
                throw new IncorrectDataFormatException("Incorrect id!");

            var model = new Model
            {
                Id = channel.Id,
                Name = channel.Name,
                About = channel.About,
                Access = channel.Access,
                SubscribersCount = await _db.ChannelsAndUsers.CountAsync(c => c.ChannelId == channel.Id)
            };

            return model;
        }

        /// <summary>
        /// Устанавливает подписку на канал для юзера.
        /// Операция только для авторизованного пользователя.
        /// </summary>
        /// <param name="channelId">
        /// ID канала
        /// </param>
        /// <param name="userId">
        /// ID пользователя, которому нужно назначить роль.
        /// </param>
        /// <returns>
        /// Результат действия.
        /// </returns>
        public async Task<bool> SubscribeToChannelAsync(int channelId, string userId)
        {
            var channel = await getChannelByChannelIDAsync(channelId);
            if (channel == null)
                throw new IncorrectDataFormatException("Incorrect id!");

            if(await _db.ChannelsAndUsers.AnyAsync(c => c.ChannelId == channel.Id && c.UserId == userId))
                throw new IncorrectDataFormatException("User already subscribed");

            if (channel.Access == Access.Public)
                return await addChanAndUsersAsync(userId, channelId, Roles.Subscriber);
            if (channel.Access == Access.Private)
                return await addChanAndUsersAsync(userId, channelId, Roles.Desirous);
            return false;
        }
        /// <summary>
        /// Устанавливает новую роль для юзера.
        /// Операция только для авторизованного пользователя и владельца данного канала.
        /// </summary>
        /// <returns>
        /// Результат действия.
        /// </returns>
        public async Task<bool> ChangeUserRoleAsync(IUserRoleModel model, string userIntiatorId)
        {
            if (!await CheckUserRightsAsync(model.ChannelId, userIntiatorId, Roles.Admin))
                throw new IncorrectDataFormatException("User does not have rights to do this!");

            return await addChanAndUsersAsync(model.UserId, model.ChannelId, model.Role);
        }

        /// <summary>
        /// Создание нового канала.
        /// Делать только для авторизованного пользователя.
        /// </summary>
        /// <param name="model">
        /// Модель канала, имплементирующего интерфейс IChannelModel.
        /// </param>
        /// <param name="userId">
        /// ID пользователя, которому нужно назначить роль.
        /// </param>
        /// <returns></returns>
        public async Task<bool> MakeNewChannelAsync(ICreateChannelModel model, string userId)
        {

            Channel channel = new Channel
            {
                Access = model.Access,
                Name = model.Name,
                About = model.About
            };
            _db.Channels.Add(channel);
            await _db.SaveChangesAsync();

            return await addChanAndUsersAsync(userId, channel.Id, Roles.Admin);    
        }

        async Task<Channel> getChannelByChannelIDAsync(int idChannel)
        {
            Channel channel = null;
            if (!_memoryCache.TryGetValue(idChannel.ToString() + "-channel", out channel))
            {
                channel = await _db.Channels.Where(c => c.Id == idChannel).FirstOrDefaultAsync();

                if (channel != null)
                {
                    _memoryCache.Set(idChannel.ToString() + "-channel", channel,
                        new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(ExpirationCacheTime)));
                }
            }
            return channel;
        }

        async Task<bool> addChanAndUsersAsync(string userId, int channelId, Roles role)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var channel = await _db.Channels.Where(c => c.Id == channelId).SingleAsync();

            _db.ChannelsAndUsers.RemoveRange(_db.ChannelsAndUsers.Where(c => c.UserId == userId && c.ChannelId == channelId));

            ChannelsAndUsers channelsAndUsers = new ChannelsAndUsers()
            {
                Channel = channel,
                User = user,
                Role = role
            };
            _db.ChannelsAndUsers.Add(channelsAndUsers);

            await _db.SaveChangesAsync();

            return true;
        }
    }
}
