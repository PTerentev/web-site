﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Enums;
using WebProject.Exceptions;

namespace WebProject.DataBase.Functions
{
    public class ChatFunctions: BaseFunctions
    {
        public ChatFunctions(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IMemoryCache memoryCache) : base(db, userManager, memoryCache)
        {

        }

        public async Task<bool> AddChatAsync(int channelId, string userId, string chatName)
        {
            if (string.IsNullOrEmpty(chatName))
                throw new IncorrectDataFormatException("Incorrect chat name!");

            if (await CheckUserRightsAsync(channelId, userId, Roles.Moderator))
            {
                var chat = new Chat()
                {
                    Channel = await _db.Channels.Where(c => c.Id == channelId).SingleAsync(),
                    Owner = await _userManager.FindByIdAsync(userId),
                    Name = chatName
                };
                _db.Chats.Add(chat);
                await _db.SaveChangesAsync();
                return true;
            }
            else
            {
                throw new IncorrectDataFormatException("User does not have rights to do this!");
            }
        }
        public async Task<bool> RemoveChatAsync(int chatId, string userId)
        {
            var chat = await _db.Chats.Where(c => c.Id == chatId).SingleAsync();

            if (await CheckUserRightsAsync(chat.ChannelId, userId, Roles.Moderator))
            {
                _db.Chats.Remove(chat);
                await _db.SaveChangesAsync();
                return true;
            }
            else
            {
                throw new IncorrectDataFormatException("User does not have rights to do this!");
            }
        }

        public async Task<bool> BlockUserInTheChatAsync(int chatId, string moderId, string userId)
        {
            if (moderId == userId)
                throw new IncorrectDataFormatException("User can not block himself!");

            var chat = await _db.Chats.Include(c => c.BlockedUsers).Where(c => c.Id == chatId).FirstAsync();

            if (await CheckUserRightsAsync(chat.ChannelId, moderId, Roles.Moderator))
            {
                if (!await _db.UserChatBlock.AnyAsync(b => b.UserId == userId && b.ChatId == chat.Id))
                {
                    _db.UserChatBlock.Add(new UserChatBlock { UserId = userId, ChatId = chat.Id });
                    await _db.SaveChangesAsync();
                }  

                return true;
            }
            else
            {
                throw new IncorrectDataFormatException("User does not have rights to do this!");
            }
        }

        public async Task<bool> UnBlockUserInTheChatAsync(int chatId, string moderId, string userId)
        {
            var chat = await _db.Chats.Include(c => c.BlockedUsers).Where(c => c.Id == chatId).FirstAsync();

            if (await CheckUserRightsAsync(chat.ChannelId, moderId, Roles.Moderator))
            {
                if (await _db.UserChatBlock.AnyAsync(b => b.UserId == userId && b.ChatId == chat.Id))
                {
                    _db.UserChatBlock.RemoveRange(_db.UserChatBlock.Where(b => b.UserId == userId && b.ChatId == chat.Id));
                    await _db.SaveChangesAsync();
                }
                return true;
            }
            else
            {
                throw new IncorrectDataFormatException("User does not have rights to do this!");
            }
        }

        public async Task<IEnumerable<int>> GetIdChatsInTheChannel(int channelId, string userId)
        {
            var channel = await _db.Channels.Include(c => c.Chats).Where(c => c.Id == channelId).FirstOrDefaultAsync();
            if (channel == null)
                throw new IncorrectDataFormatException("Wrong channel Id");

            if (channel.Access == Access.Private)
            {
                if (!await CheckUserRightsAsync(channel.Id, userId, Roles.Subscriber))
                    throw new IncorrectDataFormatException("User does not have rights to do this!");
            }

            return channel.Chats.Select(c => c.Id);
        }

    }
}
