﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Enums;
using WebProject.DataBase.Interfaces;
using WebProject.Exceptions;

namespace WebProject.DataBase.Functions
{
    public class MessageFunctions : BaseFunctions
    {
        public MessageFunctions(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IMemoryCache memoryCache) : base(db, userManager, memoryCache)
        {
        }

        public async Task<bool> SendMessage(string userId, int chatId, string text)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrWhiteSpace(text))
                throw new IncorrectDataFormatException("Incorrect text!");

            if (text.Length > 300)
                throw new IncorrectDataFormatException("Text lenght more than 300!");

            await CheckValid(userId, chatId);

            var message = new Message()
            {
                SenderId = userId,
                ChatId = chatId,
                Text = text
            };
            _db.Messages.Add(message);

            await _db.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<Model>> GetMessagesFromTheChat<Model>(string userId, int chatId, int? lastMessageId = null)
            where Model : class, IMessageModel, new()
        {
            await CheckValid(userId, chatId);

            if (lastMessageId != null)
            {
                if (await _db.Messages.AnyAsync(m => m.Id == lastMessageId && m.ChatId == chatId))
                {
                    return _db.Messages.Where(m => m.Id == lastMessageId && m.ChatId == chatId)
                        .OrderByDescending(m => m.Date)
                        .TakeWhile(m => m.Id != lastMessageId)
                        .Reverse()
                        .Select(m => new Model()
                        {
                            Text = m.Text,
                            SenderId = m.SenderId,
                            Date = m.Date.ToString(),
                            Id = m.Id
                        });
                }
                else
                    throw new IncorrectDataFormatException("This message Id does not exist in the chat!");
            }
            else
            {
                return _db.Messages.Where(m => m.ChatId == chatId)
                    .OrderBy(m => m.Date)
                    .Select(m => new Model()
                    {
                        Text = m.Text,
                        SenderId = m.SenderId,
                        Date = m.Date.ToString(),
                        Id = m.Id
                    });

            }
        }

        async Task CheckValid(string userId, int chatId)
        {
            if(!await _db.Chats.AnyAsync(c => c.Id == chatId))
                throw new IncorrectDataFormatException("Incorrect chat Id!");

            if (await _db.UserChatBlock.AnyAsync(c => c.UserId == userId && c.ChatId == chatId))
                throw new IncorrectDataFormatException("User is blocked in the chat!");

            var channelId = (await _db.Chats.Where(chat => chat.Id == chatId).SingleAsync()).ChannelId;

            if (!await CheckUserRightsAsync(channelId, userId, Roles.Subscriber))
                throw new IncorrectDataFormatException("User do not have access to the chat!");
        }

    }
}
