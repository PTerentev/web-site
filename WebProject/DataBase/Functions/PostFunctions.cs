﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Enums;
using WebProject.DataBase.Entites;
using Microsoft.EntityFrameworkCore;
using WebProject.DataBase.Interfaces;
using WebProject.Exceptions;
using Microsoft.Extensions.Caching.Memory;

namespace WebProject.DataBase.Functions
{
    /// <summary>
    /// Операции над постами.
    /// </summary>
    /// <remarks>
    /// Этот класс может добавить, удалить и изменить пост.
    /// </remarks>
    public class PostFunctions: BaseFunctions
    {
        public PostFunctions(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IMemoryCache memoryCache) : base(db, userManager, memoryCache)
        {

        }
        // формат текста нужно изменить.
        public async Task<bool> AddPostAsync(int channelId, string userId, string text)
        {

            if (!await CheckUserRightsAsync(channelId, userId, Roles.Subscriber))
                throw new IncorrectDataFormatException("User does not have rights to do this!");

            var post = new Post()
            {
                AuthorId = userId,
                Text = text,
                ChannelId = channelId
            };
                  
            _db.Posts.Add(post);
            await _db.SaveChangesAsync();

            return true;    
        }
        public async Task<bool> LikeOrCancelLikePostAsync(int postId, string userId, bool isLike = true)
        {

            var post = await GetPostByPostIDAsync(postId);
            if (post == null)
                throw new IncorrectDataFormatException("There is not such post Id in db.");

            if (post.Channel.Access == Access.Public ||
               post.Channel.Access == Access.Private && await CheckUserRightsAsync(post.Channel.Id, userId, Roles.Subscriber))
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                    throw new Exception("There is not such user Id in db.");

                if (isLike)
                {
                    if (! await _db.UserPostLike.AnyAsync(w => w.PostId == postId && w.UserId == userId))
                        _db.UserPostLike.Add(new UserPostLike { PostId = postId, UserId = userId });
                }
                else if (await _db.UserPostLike.AnyAsync(w => w.PostId == postId && w.UserId == userId))
                    _db.UserPostLike.RemoveRange(_db.UserPostLike.Where(w => w.PostId == postId && w.UserId == userId));

                await _db.SaveChangesAsync();

                return true;
            }
            return false;
        }
        public async Task<Model> GetPostAsync<Model>(int idPost, string userId)
            where Model : class, IPostModel, new()
        {
            var post = await GetPostByPostIDAsync(idPost);
            if (post == null)
                throw new IncorrectDataFormatException("There is not such post Id in db.");

            if (post.Channel.Access == Access.Private)
                if (!await CheckUserRightsAsync(post.Channel.Id, userId, Roles.Subscriber))
                    throw new IncorrectDataFormatException("User does not have rights to do this!");

            var model = new Model()
            {
                Id = post.Id,
                Author = post.Author.UserName,
                Text = post.Text,
                Date = post.Date.ToString(),
                CountLikes = post.WhoLiked?.Count() ?? 0,
                CountViews = post.CountViewers,
                IsThisUserLikedIt = post.WhoLiked.Any(w => w.UserId == userId) 
            };

            post.CountViewers++;
            _db.Posts.Update(post);
            await _db.SaveChangesAsync();

            return model;
        }
        public async Task<List<Model>> GetAllPostsFromChannelAsync<Model>(int channelId, string userId)
            where Model : class, IPostModel, new()
        {
            var channel = await _db.Channels.Where(c => c.Id == channelId).FirstOrDefaultAsync();
            if(channel == null)
                throw new IncorrectDataFormatException("There is not such channel Id in db.");

            if (channel.Access == Access.Private)
                if (!await CheckUserRightsAsync(channel.Id, userId, Roles.Subscriber))
                    throw new IncorrectDataFormatException("User does not have rights to do this!");


            var posts = (await GetPostsByChannelIDAsync(channel.Id)).OrderBy(p => p.Date).Select(post =>
            {
                post.CountViewers++;
                _db.Posts.Update(post);
                return new Model()
                {
                    Id = post.Id,
                    Author = post.Author.UserName,
                    Text = post.Text,
                    Date = post.Date.ToString(),
                    CountLikes = post.WhoLiked?.Count() ?? 0,
                    CountViews = post.CountViewers,
                    IsThisUserLikedIt = post.WhoLiked.Any(w => w.UserId == userId)
                };
            }).ToList();

            await _db.SaveChangesAsync();

            return posts;
        }
        async Task<Post> GetPostByPostIDAsync(int idPost)
        {
            Post post = null;
            if (!_memoryCache.TryGetValue(idPost.ToString() + "-post", out post))
            {
                post = await _db.Posts.
                    Include(p => p.Channel).
                    Include(p => p.Author).
                    Include(p => p.WhoLiked)
                    .Where(p => p.Id == idPost).FirstOrDefaultAsync();

                if (post != null)
                {
                    _memoryCache.Set(idPost.ToString() + "-post", post,
                        new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(ExpirationCacheTime)));
                }
            }
            return post;
        }

        async Task<List<Post>> GetPostsByChannelIDAsync(int channelId)
        {
            return await _db.Posts.
                Include(p => p.Channel).
                Include(p => p.Author).
                Include(p => p.WhoLiked).Where(p => p.ChannelId == channelId).ToListAsync();
        }
    }
}
