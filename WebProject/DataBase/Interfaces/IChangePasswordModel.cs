﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Interfaces
{
    public interface IChangePasswordModel
    {
        string NewPassword { get; set; }
        string OldPassword { get; set; }
    }
}
