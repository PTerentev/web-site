﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;

namespace WebProject.DataBase.Interfaces
{
    public interface IChannelModel
    {
        int Id { get; set; }
        string Name { get; set; }
        string About { get; set; }
        int SubscribersCount { get; set; }
        Access Access { get; set; }
    }
}
