﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;

namespace WebProject.DataBase.Interfaces
{
    public interface ICreateChannelModel
    {
        string Name { get; set; }
        string About { get; set; }
        Access Access { get; set; }
    }
}
