﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Interfaces
{
    public interface IMessageModel
    {
        int Id { get; set; }
        string SenderId { get; set; }
        string Text { get; set; }
        string Date { get; set; } 

    }
}
