﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Entites;

namespace WebProject.DataBase.Interfaces
{
    public interface IPostModel
    {
        int Id { get; set; }
        string Author { get; set; }
        string Text { get; set; }
        string Date { get; set; }
        int CountLikes { get; set; }
        int CountViews{ get; set; }
        bool IsThisUserLikedIt { get; set; }
    }
}
