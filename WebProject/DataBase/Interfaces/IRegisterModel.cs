﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Interfaces
{
    public interface IRegisterModel
    {
        string Username { get; set; }

        string Email { get; set; }

        string Password { get; set; }

    }
}
