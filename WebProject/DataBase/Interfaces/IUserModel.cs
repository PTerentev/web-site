﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.DataBase.Interfaces
{
    public interface IUserModel
    {
        string Id { get; set; }
        string UserName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        byte[] AvatarImage { get; set; }
    }
}
