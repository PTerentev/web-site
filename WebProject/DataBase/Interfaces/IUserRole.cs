﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;

namespace WebProject.DataBase.Interfaces
{
    public interface IUserRole
    {
        string UserId { get; set; }
        Roles Role { get; set; }
    }
}
