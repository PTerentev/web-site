﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.Exceptions
{
    public class IncorrectDataFormatException: Exception
    {
        public IncorrectDataFormatException(string message): base(message)
        {

        }
    }
}
