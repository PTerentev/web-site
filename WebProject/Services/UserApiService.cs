﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Interfaces;
using WebProject.Exceptions;
using WebProject.ViewModels;

namespace WebProject.Services
{
    public class UserApiService
    {
        public readonly SignInManager<ApplicationUser> SignInManager;
        public readonly UserManager<ApplicationUser> UserManager;
        private readonly TokenService _tokenService;
        public UserApiService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, TokenService tokenService)
        {
            SignInManager = signInManager ?? throw new ArgumentNullException(nameof(UserManager<ApplicationUser>));
            UserManager = userManager ?? throw new ArgumentNullException(nameof(SignInManager<ApplicationUser>));
            _tokenService = tokenService ?? throw new ArgumentNullException(nameof(TokenService));
        }

        public string GetUserIdByPrincipal(ClaimsPrincipal principal)
        {
            return principal?.Claims?.Where(c => c.Type == ClaimTypes.NameIdentifier).First()?.Value;
        }

        public async Task<UserModel> GetUserInfoAsync(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                throw new IncorrectDataFormatException("User not found");

            var model = new UserModel();
            model.UserName = user.UserName;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            return model;
        }

        public async Task<string> GetJwTokenAsync(JwtUserModel model)
        {
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
                throw new IncorrectDataFormatException("Incorret data.");

            var result = await SignInManager.CheckPasswordSignInAsync(user, model.Password, false);
            if (result.Succeeded)
            {
                return await _tokenService.GenerateToken(user);
            }
            else
            {
                throw new IncorrectDataFormatException("Incorrect password.");
            }
            
        }

        public async Task<string> GetJwTokenAsync(string id)
        {
            var user = await UserManager.FindByIdAsync(id);

            return await _tokenService.GenerateToken(user);
        }

        public async Task<string> RegisterUserAsync(RegisterModel model)
        {
            var user = new ApplicationUser()
            {
                UserName = model.Username,
                Email = model.Email
            };

            var result = await UserManager.CreateAsync(user, model.Password);

            if(result.Succeeded)
            {
                return await GetJwTokenAsync(user.Id);
            }
            else
            {
                var message = new StringBuilder();

                foreach(var str in result.Errors)
                {
                    message.Append(str.Description);
                    message.Append("\n");
                }
                throw new IncorrectDataFormatException(message.ToString());
            }
        }

        public async Task<bool> ChangeUserAsync(string username, ChangeUserModel model)
        {
            var user = await UserManager.FindByNameAsync(username);

            user.Email = model.Email ?? user.Email;
            user.FirstName = model.FirstName ?? user.FirstName;
            user.LastName = model.LastName ?? user.LastName;

            var result = await UserManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                var message = new StringBuilder();

                foreach (var str in result.Errors)
                {
                    message.Append(str.Description);
                    message.Append("\n");
                }
                throw new IncorrectDataFormatException(message.ToString());
            }
        }

    }
}
