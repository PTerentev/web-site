﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.Tokens
{
    public class JwtToken
    {
        public const string Issuer = "WebSite";
        public const string Audience = "ApiUser";
        public const string Key = "3213123123123131";
        public const int ExpirationInMinutes = 60;
    }
}
