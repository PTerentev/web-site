﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.ViewModels
{
    public class AddPostModel
    {
        [Required]
        public string Text { get; set; }
        [Required]
        public int ChannelId { get; set; }
    }
}
