﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class ChangePasswordModel: IChangePasswordModel
    {
        [Required(ErrorMessage = "Не указан пароль новый пароль")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не указан пароль старый пароль")]
        public string OldPassword { get; set; }
    }
}
