﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class ChannelModel: IChannelModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public Access Access { get; set; }
        public int SubscribersCount { get; set; }
    }
}
