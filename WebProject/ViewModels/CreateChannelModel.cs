﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class CreateChannelModel : ICreateChannelModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string About { get; set; }

        [Required]
        public Access Access { get; set; }
    }
}
