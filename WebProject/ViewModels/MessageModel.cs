﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class MessageModel: IMessageModel
    {
        public int Id { get; set; }
        public string SenderId { get; set; }
        public string Text { get; set; }
        public string Date { get; set; } 

    }
}
