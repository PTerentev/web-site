﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class PostModel: IPostModel
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
        public int CountLikes { get; set; }
        public int CountViews { get; set; }
        public bool IsThisUserLikedIt { get; set; }
    }
}
