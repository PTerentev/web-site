﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class UserChangeRoleModel : IUserRoleModel
    {
        [Required]
        public int ChannelId { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public Roles Role { get; set; }
    }
}
