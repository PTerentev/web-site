﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProject.DataBase.Enums;
using WebProject.DataBase.Interfaces;

namespace WebProject.ViewModels
{
    public class UserRole: IUserRole
    {
        public string UserId { get; set; }
        public Roles Role { get; set; }
    }
}
