﻿using Microsoft.AspNetCore.Identity;
using Moq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using WebProject.DataBase.DBContexts;
using WebProject.DataBase.Entites;
using System.Linq;

namespace XUnitTestWebProject
{
    public static class DbContextExtensions
    {
        public static void Seed(this ApplicationDbContext dbContext)
        {

            dbContext.Users.AddRange(
                new ApplicationUser()
                {
                    UserName = "228"
                }, new ApplicationUser()
                {
                    UserName = "322"
                });
            dbContext.Channels.AddRange(
                new Channel()
                {
                    Name = "First",
                    Access = WebProject.DataBase.Enums.Access.Public
                },
                new Channel()
                {
                    Name = "Second",
                    Access = WebProject.DataBase.Enums.Access.Private
                });
            dbContext.SaveChanges();
            dbContext.ChannelsAndUsers.AddRange(
                new ChannelsAndUsers()
                {
                    User = dbContext.Users.First(),
                    Channel = dbContext.Channels.First(),
                    Role = WebProject.DataBase.Enums.Roles.Admin
                },
                new ChannelsAndUsers()
                {
                    User = dbContext.Users.First(),
                    Channel = dbContext.Channels.First(),
                    Role = WebProject.DataBase.Enums.Roles.Subscriber
                },
                new ChannelsAndUsers()
                {
                    User = dbContext.Users.First(),
                    Channel = dbContext.Channels.First(),
                    Role = WebProject.DataBase.Enums.Roles.Moderator
                });
            dbContext.ChannelsAndUsers.AddRange(
                new ChannelsAndUsers()
                {
                    User = dbContext.Users.Last(),
                    Channel = dbContext.Channels.Last(),
                    Role = WebProject.DataBase.Enums.Roles.Admin
                },
                new ChannelsAndUsers()
                {
                    User = dbContext.Users.Last(),
                    Channel = dbContext.Channels.Last(),
                    Role = WebProject.DataBase.Enums.Roles.Subscriber
                },
                new ChannelsAndUsers()
                {
                    User = dbContext.Users.Last(),
                    Channel = dbContext.Channels.Last(),
                    Role = WebProject.DataBase.Enums.Roles.Moderator
                });
            dbContext.SaveChanges();
        }
    }
}
