﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using WebProject.DataBase.Entites;

namespace XUnitTestWebProject.MockObjects
{
    public class MockUserManager: UserManager<ApplicationUser>
    {
        string userid { get; set; }
        public MockUserManager(string userId): base(
                new Mock<IUserStore<ApplicationUser>>().Object,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null)
        {
            userid = userId;
        }
        public override string GetUserId(ClaimsPrincipal principal)
        {
            return userid;
        }
    }
}
