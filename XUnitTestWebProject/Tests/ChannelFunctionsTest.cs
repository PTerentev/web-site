﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Functions;
using WebProject.DataBase.Interfaces;
using Xunit;
using XUnitTestWebProject.MockObjects;

namespace XUnitTestWebProject
{
    public class ChannelFunctionsTest
    {
        /// <summary>
        /// Первый юзер владеет первым каналом(он приватный), второй - вторым(приватным)
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGetchannelOnPublicChannel()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb1");

            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            var userManager = new MockUserManager(db.Users.First().Id);

            var cf = new ChannelFunctions(db, userManager);
            var firstchannel = await cf.Getchannel<ChannelModel>(db.Channels.First().Id);
            Assert.NotNull(firstchannel);
            db.Dispose();
        }
        /// <summary>
        /// Попытка получить доступ к закрытому каналу через анонимного пользователя.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGetchannelOnPrivateChannelCPrincipalNull()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb2");
        
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.First().Id);
            var cf = new ChannelFunctions(db, userManager);
            var secondchannel = await cf.Getchannel<ChannelModel>(db.Channels.Last().Id);
            Assert.Null(secondchannel);
            db.Dispose();
        }
        /// <summary>
        /// Попытка получить доступ к закрытому каналу через пользователя, который не подписан на канал.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGetchannelOnPrivateChannelCPrincipalNotNull()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb3");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.First().Id);
            var cf = new ChannelFunctions(db, userManager);
            var secondchannel = await cf.Getchannel<ChannelModel>(db.Channels.Last().Id, cp);
            Assert.Null(secondchannel);
            db.Dispose();
        }
        /// <summary>
        /// Попытка получить доступ к закрытому каналу через пользователя, который подписан на канал.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGetchannelOnPrivateChannelCsubcriber()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb4");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.Last().Id);
            var cf = new ChannelFunctions(db, userManager);
            var secondchannel = await cf.Getchannel<ChannelModel>(db.Channels.Last().Id, cp);
            Assert.NotNull(secondchannel);
            db.Dispose();
        }
        /// <summary>
        /// Попытка подписаться на открытый канал , должен давать роль subcriber.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestSubcribeToChannelOnPublicChannel()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb5");
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);

            var userManager = new MockUserManager(db.Users.Last().Id);

            var cf = new ChannelFunctions(db, userManager);
            var sub = await cf.SubcribeToChannel(db.Channels.First().Id, cp);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.Last().Id && c.ChannelId == db.Channels.First().Id).ToList();
            
            Assert.Equal(1, cAndU.Count);
            db.Dispose();
        }
        /// <summary>
        /// Попытка подписаться на закрытый канал , должен давать роль Desirous, а не subcriber.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestSubcribeToChannelOnPrivateChannel()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb6");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);

            var userManager = new MockUserManager(db.Users.First().Id);

            var cf = new ChannelFunctions(db, userManager);
            var sub = await cf.SubcribeToChannel(db.Channels.Last().Id, cp);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.First().Id && c.ChannelId == db.Channels.Last().Id
            && c.Role == WebProject.DataBase.Enums.Roles.Desirous
            ).ToList();
            Assert.Equal(1, cAndU.Count);
            db.Dispose();
        }
        /// <summary>
        /// Попытка дать роль юзеру, через юзера, который не является админом.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGiveRoleToUserByNonAdminUser()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb7");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);       
            var userManager = new MockUserManager(db.Users.First().Id);
            db.Users.Add(new ApplicationUser()
            {
                UserName = "Vasia"
            });
            db.SaveChanges();
            var cf = new ChannelFunctions(db, userManager);
            var sub = await cf.GiveRoleToUser(cp, db.Channels.Last().Id, db.Users.First(u => u.UserName == "Vasia").Id, WebProject.DataBase.Enums.Roles.Admin);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.First(u => u.UserName == "Vasia").Id && c.ChannelId == db.Channels.Last().Id
            && c.Role == WebProject.DataBase.Enums.Roles.Admin
            ).ToList();
            Assert.Equal(0,cAndU.Count);
            db.Dispose();
        }
        [Fact]
        public async Task TestRemoveRoleToUserByNonAdminUser()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb70");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.First().Id);
            var cf = new ChannelFunctions(db, userManager);
            var sub = await cf.RemoveRoleToUser(cp, db.Channels.Last().Id, db.Users.Last().Id, WebProject.DataBase.Enums.Roles.Admin);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.Last().Id && c.ChannelId == db.Channels.Last().Id
            
            ).ToList();
            Assert.Equal(3, cAndU.Count);
            db.Dispose();
        }
        [Fact]
        public async Task TestRemoveRoleToUserByAdminUser()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb71");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.Last().Id);
            var cf = new ChannelFunctions(db, userManager);
            var subres = await cf.SubcribeToChannel(db.Channels.First().Id, cp);

            userManager = new MockUserManager(db.Users.First().Id);
            cf = new ChannelFunctions(db, userManager);
            var res = await cf.RemoveRoleToUser(cp, db.Channels.First().Id, db.Users.Last().Id, WebProject.DataBase.Enums.Roles.Subscriber);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.Last().Id 
            && c.ChannelId == db.Channels.First().Id
            ).ToList();
            Assert.Equal(0, cAndU.Count);
            db.Dispose();
        }
        /// <summary>
        /// Попытка дать роль Админа юзеру, через юзера, который является админом.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGiveRoleAdminToUserByAdminUser()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb8");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.Last().Id);
            db.Users.Add(new ApplicationUser()
            {
                UserName = "Vasia"
            });
            db.SaveChanges();
            var cf = new ChannelFunctions(db, userManager);
            var sub = await cf.GiveRoleToUser(cp, db.Channels.Last().Id, db.Users.First(u => u.UserName == "Vasia").Id, WebProject.DataBase.Enums.Roles.Admin);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.First(u => u.UserName == "Vasia").Id 
            && c.ChannelId == db.Channels.Last().Id).ToList();
            Assert.Equal(3, cAndU.Count);
            db.Dispose();
        }
        /// <summary>
        /// Попытка дать роль Модератора юзеру, через юзера, который является админом.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestGiveRoleModeratorToUserByAdminUser()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb9");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.Last().Id);
            db.Users.Add(new ApplicationUser()
            {
                UserName = "Vasia"
            });
            db.SaveChanges();
            var cf = new ChannelFunctions(db, userManager);
            var sub = await cf.GiveRoleToUser(cp, db.Channels.Last().Id, db.Users.First(u => u.UserName == "Vasia").Id, 
                WebProject.DataBase.Enums.Roles.Moderator);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.First(u => u.UserName == "Vasia").Id
            && c.ChannelId == db.Channels.Last().Id).ToList();
            Assert.Equal(2, cAndU.Count);
            db.Dispose();
        }
        /// <summary>
        /// Попытка создать новый канал
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestMakeNewChannel()
        {
            var db = DbContextMocker.GetApplicationDbContext("MockDb10");

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            var cp = new ClaimsPrincipal(idclaims);
            var userManager = new MockUserManager(db.Users.First().Id);
            var cf = new ChannelFunctions(db, userManager);
            var model = new ChannelModel()
            {
                About = "test",
                Access = WebProject.DataBase.Enums.Access.Public,
                Name = "NewCh"
            };
            var sub = await cf.MakeNewChannel(model, cp);
            var cAndU = db.ChannelsAndUsers.ToList().
            Where(c => c.UserId == db.Users.First().Id
            && c.ChannelId == db.Channels.Last().Id).ToList();
            Assert.Equal(3, cAndU.Count);
            db.Dispose();
        }
    }
}
