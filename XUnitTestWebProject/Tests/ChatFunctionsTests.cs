﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebProject.DataBase.Functions;
using WebProject.DataBase.Interfaces;
using Xunit;
using XUnitTestWebProject.MockObjects;

namespace XUnitTestWebProject.Tests
{
    public class ChatFunctionsTests
    {
        [Fact]
        public async Task TestAddChatOnPublicChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbChat1");
            var userManager = new MockUserManager(db.Users.First().Id);

            var cf = new ChatFunctions(db, userManager);
            var checkAdd = await cf.AddChatAsync(db.Channels.First().Id, cp, "test");
            var post = db.Chats.First();
            Assert.NotNull(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestAddPostOnPrivateChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbChat12");
            var userManager = new MockUserManager(db.Users.Last().Id);

            var cf = new ChatFunctions(db, userManager);
            var checkAdd = await cf.AddChatAsync(db.Channels.First().Id, cp, "test");
            var post = db.Chats.FirstOrDefault();

            Assert.Null(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestAddChatOnPrivateChannelWithAdminRights()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbChat123");
            var userManager = new MockUserManager(db.Users.Last().Id);

            var cf = new ChatFunctions(db, userManager);
            var checkAdd = await cf.AddChatAsync(db.Channels.Last().Id, cp, "test");
            var post = db.Chats.First();
            //var post = await pf.GetPost<PostModel>(db.Posts.First().Id);
            Assert.NotNull(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetChatsOnPublicChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbChat1d9sdae2");
            var userManager = new MockUserManager(db.Users.First().Id);

            var cf = new ChatFunctions(db, userManager);
            var checkAdd = await cf.AddChatAsync(db.Channels.First().Id, cp, "test");
            checkAdd = await cf.AddChatAsync(db.Channels.First().Id, cp, "test3");
            checkAdd = await cf.AddChatAsync(db.Channels.First().Id, cp, "test2");

            userManager = new MockUserManager(db.Users.Last().Id);
            cf = new ChatFunctions(db, userManager);
            var chats = await cf.GetIdChatsInTheChannel(db.Channels.First().Id);
            Assert.NotNull(chats);
            chats = await cf.GetIdChatsInTheChannel(db.Channels.First().Id, cp);
            Assert.NotNull(chats);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetChatsOnPrivateChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbChat532ssdae2");
            var userManager = new MockUserManager(db.Users.Last().Id);

            var cf = new ChatFunctions(db, userManager);
            var checkAdd = await cf.AddChatAsync(db.Channels.Last().Id, cp, "test");
            checkAdd = await cf.AddChatAsync(db.Channels.Last().Id, cp, "test3");
            checkAdd = await cf.AddChatAsync(db.Channels.Last().Id, cp, "test2");
            Assert.Equal(3, db.Chats.Count());

            userManager = new MockUserManager(db.Users.First().Id);
            cf = new ChatFunctions(db, userManager);
            var chats = await cf.GetIdChatsInTheChannel(db.Channels.Last().Id);
            Assert.Null(chats);
            chats = await cf.GetIdChatsInTheChannel(db.Channels.Last().Id, cp);
            Assert.Null(chats);
            db.Dispose();
        }
        
    }
}
