﻿using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebProject.DataBase.Entites;
using WebProject.DataBase.Functions;
using WebProject.DataBase.Interfaces;
using Xunit;
using XUnitTestWebProject.MockObjects;

namespace XUnitTestWebProject
{
    public class PostFunctionsTests
    {
        [Fact]
        public async Task TestAddPostOnPublicChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost");
            var userManager = new MockUserManager(db.Users.First().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.First().Id, cp, "Первый пост");
            var post = db.Posts.First();
            //var post = await pf.GetPost<PostModel>(db.Posts.First().Id);
            Assert.NotNull(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestAddPostOnPrivateChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost1");
            var userManager = new MockUserManager(db.Users.First().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");
            var post = db.Posts.FirstOrDefault();
            //var post = await pf.GetPost<PostModel>(db.Posts.First().Id);
            Assert.Null(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestAddPostOnPrivateChannelWithAdminRights()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost12");
            var userManager = new MockUserManager(db.Users.Last().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");
            var post = db.Posts.FirstOrDefault();
            //var post = await pf.GetPost<PostModel>(db.Posts.First().Id);
            Assert.NotNull(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetPostsOnPublicChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost1d9sdae2");
            var userManager = new MockUserManager(db.Users.First().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd1 = await pf.AddPost(db.Channels.First().Id, cp, "Первый пост");
            var checkAdd2 = await pf.AddPost(db.Channels.First().Id, cp, "Второй пост");
            var checkAdd3 = await pf.AddPost(db.Channels.First().Id, cp, "Третий пост");
            var posts = await pf.GetAllPostsFromChannel<PostModel>(db.Channels.First().Id);
            Assert.Equal(3, posts.Count);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetPostsOnPrivateChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost532ssdae2");
            var userManager = new MockUserManager(db.Users.Last().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd1 = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");
            var checkAdd2 = await pf.AddPost(db.Channels.Last().Id, cp, "Второй пост");
            var checkAdd3 = await pf.AddPost(db.Channels.Last().Id, cp, "Третий пост");

            userManager = new MockUserManager(db.Users.First().Id);
            pf = new PostFunctions(db, userManager);
            var posts1 = await pf.GetAllPostsFromChannel<PostModel>(db.Channels.Last().Id);
            Assert.Null(posts1);
            var posts2 = await pf.GetAllPostsFromChannel<PostModel>(db.Channels.Last().Id, cp);
            Assert.Null(posts2);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetPostsOnPrivateChannelWithUserSubcriber()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost23ssdae2");
            var userManager = new MockUserManager(db.Users.Last().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd1 = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");
            var checkAdd2 = await pf.AddPost(db.Channels.Last().Id, cp, "Второй пост");
            var checkAdd3 = await pf.AddPost(db.Channels.Last().Id, cp, "Третий пост");


            var posts1 = await pf.GetAllPostsFromChannel<PostModel>(db.Channels.Last().Id);
            Assert.Null(posts1);
            var posts2 = await pf.GetAllPostsFromChannel<PostModel>(db.Channels.Last().Id, cp);
            Assert.NotNull(posts2);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetPostOnPublicChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost1sd2");
            var userManager = new MockUserManager(db.Users.First().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.First().Id, cp, "Первый пост");
            var post = await pf.GetPost<PostModel>(db.Posts.First().Id);
            Assert.NotNull(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetPostOnPrivateChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost122");
            var userManager = new MockUserManager(db.Users.Last().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");

            userManager = new MockUserManager(db.Users.First().Id);
            pf = new PostFunctions(db, userManager);
            var post = await pf.GetPost<PostModel>(db.Posts.First().Id, cp);
            Assert.Null(post);
            db.Dispose();
        }
        [Fact]
        public async Task TestGetPostOnPrivateChannelWithUserSubcriber()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPost1232");
            var userManager = new MockUserManager(db.Users.Last().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");

            var post = await pf.GetPost<PostModel>(db.Posts.First().Id, cp);
            Assert.NotNull(post);
            db.Dispose();
        }

        [Fact]
        public async Task TestLikePostByNonSubUserOnPublicChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPdaost1fsafa");
            var userManager = new MockUserManager(db.Users.First().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.First().Id, cp, "Первый пост");

            var res1 = await pf.LikeOrCancelLikePost(db.Posts.First().Id, cp);
            Assert.True(res1);

            userManager = new MockUserManager(db.Users.Last().Id);
            pf = new PostFunctions(db, userManager);
            var res2 = await pf.LikeOrCancelLikePost(db.Posts.First().Id, cp);
            Assert.True(res2);
            db.Dispose();
        }
        [Fact]
        public async Task TestLikePostByNonSubUserOnPrivateChannel()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "MockUser"),
            };
            ClaimsIdentity idclaims = new ClaimsIdentity(claims, "ApplicationCookie",
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var cp = new ClaimsPrincipal(idclaims);

            var db = DbContextMocker.GetApplicationDbContext("MockDbPosdsat1fsafa");
            var userManager = new MockUserManager(db.Users.Last().Id);
            var pf = new PostFunctions(db, userManager);
            var checkAdd = await pf.AddPost(db.Channels.Last().Id, cp, "Первый пост");

            var res1 = await pf.LikeOrCancelLikePost(db.Posts.First().Id, cp);
            Assert.True(res1);

            userManager = new MockUserManager(db.Users.First().Id);
            pf = new PostFunctions(db, userManager);
            var res2 = await pf.LikeOrCancelLikePost(db.Posts.First().Id, cp);
            Assert.False(res2);
            db.Dispose();
        }
    }
}
